package com.tp.sampathw;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    public void atLeastThreeChars() {
        App app = new App();
        Assert.assertFalse( app.validatePassword("tr"));
        Assert.assertTrue( app.validatePassword("test123"));
    }

    @Test
    public void allNumericTest(){
        App app = new App();
        Assert.assertFalse(app.validatePassword("1234"));
        Assert.assertTrue(app.validatePassword("test123"));
    }

    @Test
    public void allStringTest(){
        App app = new App();
        Assert.assertTrue(app.validatePassword("test123"));
        Assert.assertFalse(app.validatePassword("test"));
    }
}
