package com.tp.sampathw.model;

public class MinLengthValidator implements IValidator {
    private int minLength;

    public MinLengthValidator(int minLength) {
        this.minLength = minLength;
    }

    @Override
    public boolean validatePassword(String password) {
        return password.length() > minLength;
    }
}
