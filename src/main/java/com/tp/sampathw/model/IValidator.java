package com.tp.sampathw.model;

public interface IValidator {

    /**
     * Validate password
     *
     * @param password
     * @return
     */
    boolean validatePassword(String password);
}
