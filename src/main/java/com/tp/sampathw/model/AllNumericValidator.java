package com.tp.sampathw.model;

import org.apache.commons.lang3.StringUtils;

public class AllNumericValidator implements IValidator {

    @Override
    public boolean validatePassword(String password) {
        return !StringUtils.isNumeric(password);
    }
}
