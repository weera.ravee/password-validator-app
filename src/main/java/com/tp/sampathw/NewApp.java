package com.tp.sampathw;

import com.tp.sampathw.model.AllNumericValidator;
import com.tp.sampathw.model.IValidator;
import com.tp.sampathw.model.MinLengthValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class NewApp extends AbstractApp{

    @Override
    public List<IValidator> getValidatorList() {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new AllNumericValidator());
        validators.add(new MinLengthValidator(5));
        validators.add(new AllNumericValidator());
        return validators;
    }
}
