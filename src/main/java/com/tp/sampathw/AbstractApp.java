package com.tp.sampathw;

import com.tp.sampathw.model.IValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public abstract  class AbstractApp {
    private static Logger logger = LogManager.getLogger(AbstractApp.class);

    public boolean validatePassword(String password){
       boolean output = getValidatorList().parallelStream().allMatch(validator -> validator.validatePassword(password));
        logger.info("input: {}, output: {}", password, output);
        return  output;
    }


    public abstract List<IValidator> getValidatorList();

}
