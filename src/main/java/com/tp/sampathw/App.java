package com.tp.sampathw;

import com.tp.sampathw.model.AllNumericValidator;
import com.tp.sampathw.model.IValidator;
import com.tp.sampathw.model.MinLengthValidator;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App extends AbstractApp{

    @Override
    public List<IValidator> getValidatorList() {
        List<IValidator> validators = new ArrayList<>();
        validators.add(new AllNumericValidator());
        validators.add(new MinLengthValidator(3));
        return validators;
    }
}
